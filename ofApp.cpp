#include "ofApp.h"

ofVec2f dirs[4] = { ofVec2f(1, 0), ofVec2f(0, 1), ofVec2f(-1, 0), ofVec2f(0, -1)  };

ofColor red    = ofColor(255, 0, 0),
        yellow = ofColor(255, 200, 0),
        black  = ofColor(0, 0, 0),
        white  = ofColor(255,255, 255),
        green  = ofColor(0, 255, 100),
        blue   = ofColor(10, 55, 255),
        pink   = ofColor(255, 0, 255);

#define BACK 0
#define LEVEL 1
#define PLAYER 2
#define DOOR 3
#define WIN 4
#define LOSE 5


const int levels = 9;
//                BACK    LEVEL  PLAYER DOOR   WIN     LOSE
ofColor c1[6] = { white , black, red ,  black, yellow, red },
        c2[6] = { yellow, black, red,   black, green,  yellow },
        c3[6] = { black,  yellow,blue,  green, red,    blue },
        c4[6] = { red,    white, black, blue,  blue,   red },
        c5[6] = { black,  green, white, red,   blue,   white},
        c6[6] = { blue,   red,   yellow,white, black,  white },
        c7[6] = { black,  red,   white, yellow,white,  black },
        c8[6] = { black,  white, green, white, green,  black },
        c9[6] = { white,  red,   black, yellow,yellow, black };

ofColor* colors[levels] = { c1, c2, c3, c4, c5, c6, c7, c8, c9};

float movespeeds[levels]    = { 200.f, 225.f, 250.f, 300.f, 360.f, 350.f, 380.f, 270.f, 300.f },
      rotspeeds[levels]     = {  1.6f,  1.7f,  1.9f,   2.f,  2.4f,  2.2f,  2.2f,  2.8f,  2.4f },
      closingspeeds[levels] = {   .32f,  .42f,  .54f,   .5f,  .62f,  .58f,   .6f,  .66f,  .68f };
int lengths[levels]         = {     8,    12,    16,    20,    16,    20,    24,    12,    16 };
ofImage img;
const int maxlength = 25;
const float rectsize = 500.f;
int screenshots = 0;

enum State {Trans, Init, Start, Move, Turn, End};

int width, height;

ofPolyline level, playerline, oldlevel, newlevel;
ofVec2f player = ofVec2f(0.0f, 0.0f);
ofVec2f rotplayer = ofVec2f(0.0f, 0.0f);
ofEasyCam cam;

State state = Init;

float turnrandom = 0.2f;


int currentlength = 0;
int progress = 0;
int step = 0;
float score = 0.f;

float distancebias = 50.f;
float targetscore = .5f;

float idletime = 0.f;

float walltick = 0.0f;
float transtick = 0.0f;
float transrate = 1.4f;
float resultrate = 1.f;

float camerastart, oldcamera;
ofColor lastcolor = white;


bool noinput = true, 
     leftmousedown = false,
     leftmouseup = false,
     rightmousedown = false,
     rightmouseup = false;

void initlevels() {
  for (int i = 0; i < maxlength; i++) {
    level.addVertex(ofVec3f(0, 0, 0));
    newlevel.addVertex(ofVec3f(0, 0, 0));
    oldlevel.addVertex(ofVec3f(0, 0, 0));
  }
}

void assignnewlevel(int length, float s, float r) {
  ofPoint p, start, average;
  float endingthreshold = 30.f;
  float grid = 2.f * s  / (float) length;
  float distance;
  
  start.set((-s / 2.0f) + ofRandom(0.f, grid), (-s / 2.0f) - ofRandom(0.f, grid));
  average.set(0.f,0.f);

  for (int i = 0; i < length + 1; i++) {
    float t = (float)i / ((float)length + 1.0f);
    distance = t < 0.5f ? ((0.5f - t) / 0.5f) : ((t - 0.5f) / 0.5f);
    distance += 0.1f + (distance < 0.5f ? ofRandom(0.f, 0.1f) : 0);
    if (i == 0 || i == length) {
      p.set(start.x, start.y);
    }
    else {
      ofVec2f dir = dirs[(i - 1) % 4];
      dir = dir.getRotatedRad(ofRandom(-r, r)).getNormalized();
      dir = p + dir * s * distance;
      if (i == length - 1) // last angle smoothing
        dir.set(ofClamp(dir.x, newlevel[0].x - endingthreshold, newlevel[0].x + endingthreshold), dir.y);
      p.set(dir);
    }
    newlevel[i] = p;
    average.set(average.x + p.x, average.y + p.y);
  }

  average.set(average.x / (float)length, average.y / (float)length);
  // bring center of mass to center
  for (int i = 0; i < length + 1; i++)
    newlevel[i] = ofVec2f(
      ofClamp(newlevel[i].x - average.x, -s/1.9f, s/1.9f), 
      ofClamp(newlevel[i].y - average.y, -s / 1.9f, s / 1.9f)
      );
}
void bufferoldlevel() {
  for (int i = 0; i < maxlength; i++)
    oldlevel[i] = level[i];
}

void resetcamera() {
  cam.setPosition(0, 0, 500);
  cam.setFov(80.f);
  cam.setOrientation(ofVec3f(0, 0, camerastart));
}

void cameramovement() {
  if(state == Init || state == End ) resetcamera();
  else if( state == Trans) cam.setOrientation(ofVec3f(0, 0, ofLerp(oldcamera, camerastart, transtick)));
  else{
    switch (progress) {
      case 0: 
        break;
      case 1:
        switch (state) {
          case Move: cam.roll(0.01f); break;
          case Turn: cam.roll(0.0061f); break;
          default: break;
        }
        break;
      case 2:
        switch (state) {
          case Move: cam.setPosition(player.x, player.y, 500); break;
          case Turn:
            cam.rotate(57.2958f*rotspeeds[progress]*ofGetLastFrameTime(), ofVec3f(0, 0, 1));
            cam.setFov(80.f*walltick+20.f);
            break;
          default: break;
        }
        break;
      case 3:
        switch (state) {
          case Move: break;
          case Turn: cam.rotate(-57.2958f*rotspeeds[progress]*ofGetLastFrameTime(), ofVec3f(0, 0, 1)); break;
          default: break;
        }
        break;
      case 4:
        switch (state) {
          case Move:
            cam.setFov(60.f + 10.f * (walltick));
            cam.setPosition(player.x, player.y, 500);
            cam.lookAt(player, rotplayer.rotated(90.f*walltick) + ofGetUnixTime());
            break;
          case Turn:
            cam.setPosition(player.x+100.f*walltick, player.y, 500);
            cam.rotate(walltick*257.2958f*rotspeeds[progress]*ofGetLastFrameTime(), ofVec3f(0, 0, 1));
            cam.setFov(60.f + 10.f * (1.f - walltick));
            break;
          default: break;
        }
        break;
      case 5:
        switch (state) {
          case Move:
            cam.setFov(40.f + 40.f * (walltick));
            cam.setPosition(player.x, player.y +200.f, 500);
            cam.lookAt(player, rotplayer);
            break;
          case Turn:
            cam.lookAt(player, rotplayer);
            cam.setFov(40.f + 40.f * (1.f-walltick));
            break;
          default: break;
        }
        break;
      case 6:
        switch (state) {
          case Move:
            cam.rotate(57.2958f*rotspeeds[progress] * ofGetLastFrameTime(), ofVec3f(0, 0, 1));
            cam.setFov(80.f - 60.f * (walltick));
            cam.setPosition(player.x, player.y - 60.f * (walltick), 500);
            break;
          case Turn:
            cam.rotate(-16.2958f*rotspeeds[progress] * ofGetLastFrameTime(), ofVec3f(0, 0, 1));
            cam.setFov(80.f - 40.f * (1.f - walltick));
            break;
          default: break;
        }
        break;
      case 7:
        switch (state) {
          case Move:
            cam.rotate(74.2958f*rotspeeds[progress] * ofGetLastFrameTime(), ofVec3f(0, 0, 1));
            cam.setFov(80.f - 10.f * (walltick));
            break;
          case Turn:
            cam.rotate(-57.2958f*rotspeeds[progress] * ofGetLastFrameTime(), ofVec3f(0, 0, 1));
            cam.setFov(80.f + 30.f * (1.f - walltick));
            break;
          default: break;
        }
        break;
      case 8:
        switch (state) {
        case Move:
          cam.setPosition(ofVec3f(0, 0, 200));
          cam.setFov(80.f - 30.f * (1.f - walltick));
          cam.lookAt(player, rotplayer);
          break;
        case Turn:
          cam.setPosition(ofVec3f(0, 0, 500).getInterpolated(player, walltick));
          cam.setFov(80.f - 30.f * (walltick));
          cam.setOrientation(ofVec3f(0, 0, camerastart));
          break;
        default: break;
        }
      break;
    }
  }
}

float evaluate() {
  float r = 1.f;
  float div = 1.f / currentlength;
  for (int i = 0; i < currentlength + 1; i++) {
    r -= div * ofClamp(level[i].distance(playerline[i]) / distancebias, 0.f, 1.f );
  }
  return r;
}

void drawdoor(int side, float t, float angle) {
  ofPushMatrix();
  ofTranslate(player);
  ofRotateZ(angle);
  ofSetColor(colors[progress][DOOR]);
  ofBeginShape();
  ofVertex(-width*2, side * height * 2 * t);
  ofVertex( width*2, side * height * 2 * t);
  ofVertex( width*2, side * height * 2);
  ofVertex(-width*2, side * height * 2);
  ofEndShape();
  ofPopMatrix();
}

ofColor colorlerp(ofColor a, ofColor b, float t) {
  return ofColor(ofLerp(a.r, b.r, t), ofLerp(a.g, b.g, t), ofLerp(a.b, b.b, t));
}

void lerptonew(float t) {
  for (int i = 0; i < maxlength; i++)
    level[i] =
      i > currentlength
        ? oldlevel[ currentlength ].interpolated( newlevel[ currentlength ], t)
        : oldlevel[i].interpolated( newlevel[i], t);
}

void ofApp::setup(){
  initlevels();
  width = ofGetScreenWidth();
  height = ofGetScreenHeight();
  ofDisableSmoothing();
  ofDisableAntiAliasing();
  ofHideCursor();
  cam.disableMouseInput();
  cam.setPosition(0, rectsize, 500);
  cam.setFov(80.f);
  camerastart = floor(ofRandom(0.f, 1.f) * 4)*90;
  cam.setOrientation(ofVec3f(0, 0, camerastart));
}

void ofApp::update(){
  cameramovement();
  if (noinput) {
    idletime += ofGetLastFrameTime();
    if (idletime > 30.f) {
      progress = progress == 0 ? progress : progress - 1;
      currentlength = lengths[progress];
      state = Init;
      idletime = 0.0f;
    }
  }
  else 
      idletime = 0.f;

  switch (state) {
    case Init:
      oldcamera = camerastart;
      currentlength = lengths[progress];
      camerastart = floor(ofRandom(0.f, 1.f) * 4) * 90;
      assignnewlevel(currentlength, rectsize, turnrandom);
      bufferoldlevel();
      playerline.clear();
      player.set(newlevel[0] - ofVec2f(0.f, -1.f) * rectsize);
      score = 0.f;
      rotplayer = ofVec2f(0.f, -1.f);
      step = -1;
      walltick = 0.0f;
      transtick = 0.0f;
      state = Trans;
      break;
    case Trans:
      transtick = ofClamp(transtick + transrate*ofGetLastFrameTime(), 0.f, 1.f);
      lerptonew(transtick);
      ofSetBackgroundColor(colorlerp(colors[progress == 0 ? 0 : progress - 1][BACK], colors[progress][BACK], transtick));
      if (transtick == 1.f) state = Start;
      break;
    case Start:
      if (leftmouseup) state = Move;
      else if (rightmouseup) state = Init;
      break;
    case Move:
      player.set(player + rotplayer*movespeeds[progress] *ofGetLastFrameTime());
      if (leftmousedown) {
        if (progress == 0) walltick = 0.1f;
        state = Turn;
        playerline.addVertex(player);
        step += 1;
        if (step == currentlength) {
          state = End;
          score = evaluate();
          if (score > targetscore) {
            ofSetBackgroundColor(colors[(progress+1)%levels][BACK] );
            lastcolor = colors[progress][BACK];
            progress = (progress+1)%levels;
          }
          //if (score > targetscore) 
          else ofSetBackgroundColor(colors[progress][DOOR]);
          //ofLog() << score;
         }
      }
      if (rightmouseup || player.distance(ofVec2f(0.f, 0.f)) > width * 0.5f) {
        state = End;
        resultrate = 1.f;
      }
      walltick = ofClamp(walltick - 4.f*closingspeeds[progress] * ofGetLastFrameTime(), 0.0f, 1.0f);
      break;
    case Turn:
      if (leftmousedown) {
        walltick = ofClamp(walltick + closingspeeds[progress] * ofGetLastFrameTime(), 0.0f, 1.0f);
        if (walltick == 1.0f) {
          state = End;
          walltick = 0.96f;
        }
        else rotplayer = rotplayer.getRotatedRad(rotspeeds[progress]*ofGetLastFrameTime());
      }
      else
        state = Move;
      break;
    case End:
      walltick = walltick + resultrate * ofGetLastFrameTime() - floor(walltick + closingspeeds[progress] * ofGetLastFrameTime());
      if (playerline.size() != currentlength+1 && walltick > 0.95f) state = Init;
      if (rightmouseup) state = Init;
      break;
  }
  if(rightmouseup) rightmouseup = false;
  if(leftmouseup) leftmouseup = false;
}

void drawlevel() {
  ofSetLineWidth(4.0f);
  ofSetColor(colors[progress][LEVEL]);
  for (int i = 0; i < currentlength; i++) {
    ofDrawLine(level[i], level[i + 1]);
  }
}
void drawplayer() {
  if (state != Trans) {
    ofSetLineWidth(6.0f);
    ofSetColor(colors[progress][PLAYER]);
    playerline.draw();
    if (step == -1)
      ofDrawLine(level[0] - ofVec2f(0.f, -1.f) * rectsize, player);
    //ofDrawLine(player - rotplayer* 30.f, player);
    else
      ofDrawLine(playerline[step], player);
  }
}
void drawdoors() {
  ofVec2f v(-1.f, -1.f);
  float a = v.angle(rotplayer) + 45.f;
  float t = pow(1.0f - walltick, 10);
  drawdoor(-1, t, a);
  drawdoor(1, t, a);
}
void drawresult() {
  float t;
  if (score > targetscore) {
    t = 0.5f + 0.5f * sin(walltick*PI*2.f);
    ofSetBackgroundColor(colorlerp(colors[progress][BACK], colors[progress][WIN], t));
  }
  else {
    t = walltick < 0.5f ? ofLerp(0.0f, 1.0f, walltick*2.f) : ofLerp(1.0f, 0.f, (walltick - 0.5f) *2.f);
    ofSetBackgroundColor(colors[progress][0]);
  }
  ofSetColor(colors[progress][PLAYER]);
  for (int i = 0; i < currentlength; i++) {
    ofPoint a1 = level[i];
    ofPoint a2 = level[i + 1];
    ofPoint b1 = playerline[  i % playerline.size()     ];
    ofPoint b2 = playerline[ (i + 1) % playerline.size()];
    ofDrawLine(a1.getInterpolated(b1, t), a2.getInterpolated(b2, t));
  }
}

void ofApp::draw(){
  cam.begin();
  if (state == End && playerline.size() == currentlength+1) {
    drawresult();
  }
  else{
    drawlevel();
    drawplayer();
    if (state == Move || state == Turn || state == End )
      drawdoors();
  }
  cam.end();
}

void ofApp::mousePressed(int x, int y, int button){
  if (button == 0) leftmousedown = true;
  if (button == 2) rightmousedown = true;
  if (button == 1) {
    progress = (progress + 1) % levels;
    state = Init;
  }
  noinput = false;
}
void ofApp::mouseReleased(int x, int y, int button){
  if (button == 0) {
    leftmousedown = false;
    leftmouseup = true;
  }
  if (button == 2) {
    rightmousedown = false;
    rightmouseup = true;
  }
  noinput = true;
}
void ofApp::keyPressed(int key){
  if (key == 'x') {
    img.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
    img.save("mvvm-" + std::to_string(screenshots) + ".png");
    screenshots++;
  }

}
void ofApp::mouseMoved(int x, int y ){}
void ofApp::keyReleased(int key){}
void ofApp::mouseDragged(int x, int y, int button){}
void ofApp::mouseEntered(int x, int y){}
void ofApp::mouseExited(int x, int y){}
void ofApp::windowResized(int w, int h) {}
void ofApp::gotMessage(ofMessage msg){}
void ofApp::dragEvent(ofDragInfo dragInfo) {} 
